H Investments International Pty Ltd is owned and managed by John Hayson. In the late 1950's, John's grandfather started a small shopping centre which was extended by his mother and sold in 1986. Northbridge Plaza was purchased in 1987 and John joined the business on a full time basis after a career in investment banking. John and his mother went on to larger retail and residential development. John's mother passed away in 2010.

Website : http://www.hinvestments.net
